package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AnswerGeneratorUnitTest {

    @Test
    void should_generate_4_length_answer_string() {
        AnswerGenerator answerGenerator = new AnswerGenerator();

        assertEquals(4, answerGenerator.generate().length());
    }
}
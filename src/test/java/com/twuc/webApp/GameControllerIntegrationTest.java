package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class GameControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_get_status_201_and_location_when_create_game() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/games")
                    .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.header().string("Location", "/api/games/1"));
    }

    @Test
    void should_get_the_status_of_the_game() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/games/996"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.header().string("Content-Type", "application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.id").value(is(996)))
                .andExpect(jsonPath("$.answer").value(is("1024")));
    }

    @Test
    void should_get_404_if_game_does_not_exist() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/games/2008"))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(MockMvcResultMatchers.content().string("Game not found"));
    }

    @Test
    void should_get_the_game_answer_and_the_hint() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/996")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content("{ \"answer\": \"2048\" }"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.header().string("Content-Type", "application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.hint").value(is("1A2B")))
                .andExpect(jsonPath("$.correct").value(is(false)));
    }

    @Test
    void should_get_the_correct_hint_when_the_guess_is_correct() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/996")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content("{ \"answer\": \"1024\" }"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.header().string("Content-Type", "application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.hint").value(is("4A0B")))
                .andExpect(jsonPath("$.correct").value(is(true)));
    }

    @Test
    void should_get_400_when_the_argument_is_illegal() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/996")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{ \"answer\": \"10245\" }"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
}
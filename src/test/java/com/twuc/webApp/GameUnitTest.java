package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameUnitTest {
    @Test
    void should_get_1A2B_when_answer_is_5234_and_guess_is_5346() {
        Game game = new Game("5234", 1L);

        assertEquals("1A2B", game.getHint("5346"));
    }

    @Test
    void should_get_1A2B_when_answer_is_5234_and_guess_is_5234() {
        Game game = new Game("5234", 1L);

        assertEquals("4A0B", game.getHint("5234"));
    }

    @Test
    void should_get_1A2B_when_answer_is_5234_and_guess_is_7890() {
        Game game = new Game("5234", 1L);

        assertEquals("0A0B", game.getHint("7890"));
    }
}
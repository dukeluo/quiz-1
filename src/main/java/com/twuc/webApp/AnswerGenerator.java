package com.twuc.webApp;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class AnswerGenerator {

    public String generate() {
        String result = "";

        for (int i = 0; i < 4; i++) {
            result += new Random().nextInt(10);
        }
        return result;
    }
}

package com.twuc.webApp;

public class CheckRequest {
    private String answer;

    public CheckRequest(String answer) {
        this.answer = answer;
    }

    public CheckRequest() {
    }

    public String getAnswer() {
        return answer;
    }
}

package com.twuc.webApp;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
class GameRepository {
    private List<Game> storage = new ArrayList<Game>();

    {
        storage.add(new Game("1024", 996L));
    }

    public void addGame(Game game) {
        storage.add(game);
    }

    public Optional<Game> findById(Long id) {
        return storage.stream().filter(g -> g.getId().equals(id)).findFirst();
    }
}

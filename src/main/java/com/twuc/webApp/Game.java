package com.twuc.webApp;

public class Game {
    private final String answer;
    private final Long id;

    public Game(String answer, Long id) {
        this.answer = answer;
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public Long getId() {
        return id;
    }

    public String getHint(String guess) {
        String[] answerArray = answer.split("");
        String[] guessArray = guess.split("");
        int aCount = 0;
        int bCount = 0;

        for (int i = 0; i < answerArray.length; i++) {
            for (int j = 0; j < guessArray.length; j++) {
                if (answerArray[i].equals(guessArray[j])) {
                    bCount += 1;
                    if (i == j) {
                        aCount += 1;
                    }
                }
            }
        }
        bCount -= aCount;
        return String.format("%dA%dB", aCount, bCount);
    }
}

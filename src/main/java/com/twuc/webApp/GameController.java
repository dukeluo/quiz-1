package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@RestController
@CrossOrigin
public class GameController {

    private Long gameId = 0L;
    @Autowired
    private AnswerGenerator generator;

    @Autowired
    private GameRepository gameRepository;

    @PostMapping("/api/games")
    public ResponseEntity<String> createGame() {
        Game game = new Game(generator.generate(), gameId);

        gameId += 1;
        gameRepository.addGame(game);
        return ResponseEntity.status(HttpStatus.CREATED)
                .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.LOCATION)
                .header("Location", String.format("/api/games/%d", gameId))
                .body("Create a game success");
    }

    @GetMapping("/api/games/{gameId}")
    public ResponseEntity<Game> getGame(@PathVariable("gameId") Long gameId) {
        Game game = gameRepository.findById(gameId).get();
        HttpHeaders headers = new HttpHeaders();

        return new ResponseEntity<Game>(
                game,
                headers,
                HttpStatus.OK);
    }

    @PatchMapping("/api/games/{gameId}")
    public ResponseEntity<CheckResponse> checkAnswer(@PathVariable("gameId") Long gameId, @RequestBody CheckRequest request) {
        Game game = gameRepository.findById(gameId-1).get();
        String guess = request.getAnswer();

        if (guess.length() > 4) {
            throw new IllegalArgumentException();
        }

        String hint = game.getHint(guess);
        String answer = game.getAnswer();
        CheckResponse checkResponse = new CheckResponse(hint, guess.equals(answer));
        HttpHeaders headers = new HttpHeaders();

        return new ResponseEntity<CheckResponse>(
                checkResponse,
                headers,
                HttpStatus.OK);
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<String> noSuchElementExceptionHandler() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("Game not found");
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> illegalArgumentExceptionHandler() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body("Illegal argument");
    }
}

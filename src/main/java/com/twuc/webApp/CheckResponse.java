package com.twuc.webApp;

public class CheckResponse {
    private String hint;
    private Boolean correct;

    public CheckResponse(String hint, Boolean correct) {
        this.hint = hint;
        this.correct = correct;
    }

    public CheckResponse() {
    }

    public String getHint() {
        return hint;
    }

    public Boolean getCorrect() {
        return correct;
    }
}
